// Copyright (c) 2015, One Off Code LLC., All Rights Reserved

package execon

import (
	"log"
	"math/rand"
	"testing"
	"time"

	"golang.org/x/net/context"
)

type MyFlowFuncs struct{}

// MyFlow is a userspace structure which defines the workflow
type MyFlow struct {
	Init    ExeConTask
	Sleep   ExeConTask
	Random2 ExeConTask `params:"RANDVAL" funcname:"FnRandom"`
	OddEven ExeConTask `params:"RANDVAL" switch:"{\"ODD\":\"Odd\",\"EVEN\":\"Even\",\"ELSE\":\"Error\"}"`
	Odd     ExeConTask
	Even    ExeConTask
	Finish  ExeConTask
	Error   ExeConTask
	MyFlowFuncs
	err error
}

func (p MyFlowFuncs) Register(ctx context.Context) context.Context {
	log.Printf("Registering: ...")
	return ctx
}

func (p *MyFlow) Execute(ctx context.Context) context.Context {
	log.Printf("Running: ...")
	return Execute(context.WithValue(ctx, "SELF", p))
}

func (p MyFlowFuncs) FnInit(ctx context.Context) (context.Context, SwitchValue) {
	log.Printf("Init: ...")
	rand.Seed(time.Now().UnixNano())
	return ctx, "NEXT"
}
func (p MyFlowFuncs) FnFinish(ctx context.Context) (context.Context, SwitchValue) {
	log.Printf("Finish: ...")
	return ctx, "DONE"
}
func (p MyFlowFuncs) FnError(ctx context.Context) (context.Context, SwitchValue) {
	log.Printf("Error: ...")
	return ctx, "ERROR"
}
func (p MyFlowFuncs) FnSleep(ctx context.Context) (context.Context, SwitchValue) {
	log.Printf("Sleep: ...")
	n := ctx.Value("sleep")
	if n != nil {
		time.Sleep(time.Duration(n.(int)) * time.Second)
	}
	return ctx, "NEXT"
}
func (p MyFlowFuncs) FnRandom(ctx context.Context, saveName string) (context.Context, SwitchValue) {
	log.Printf("Random: ...")
	return context.WithValue(ctx, saveName, rand.Int()), "NEXT"
}
func (p MyFlowFuncs) FnOddEven(ctx context.Context, saveName string) (context.Context, SwitchValue) {
	log.Printf("OddEven: ...")
	n := ctx.Value(saveName).(int)
	if n%2 == 0 {
		return ctx, "EVEN"
	}
	return ctx, "ODD"
}
func (p MyFlowFuncs) FnEven(ctx context.Context) (context.Context, SwitchValue) {
	log.Printf("Even: ...")
	return ctx, "Finish"
}
func (p MyFlowFuncs) FnOdd(ctx context.Context) (context.Context, SwitchValue) {
	log.Printf("Odd: ...")
	return ctx, "Finish"
}

func TestSimple(t *testing.T) {
	p := &MyFlow{}
	p = Prepare(p).(*MyFlow)
	log.Printf("EXPRESS: %#v", *p)
	ctx := context.TODO()
	retctx := p.Execute(ctx)
	log.Printf("RESULTS: %#v - UUID:%#v, SHORTID:%#v - %#v", retctx, retctx.Value("UUID"), retctx.Value("SHORTID"), *p)
}

func TestBackground(t *testing.T) {
	p := &MyFlow{}
	p = Prepare(p).(*MyFlow)
	log.Printf("EXPRESS: %#v", *p)
	ctx := context.Background()
	retctx := p.Execute(ctx)
	log.Printf("RESULTS: %#v - UUID:%#v, SHORTID:%#v - %#v", retctx, retctx.Value("UUID"), retctx.Value("SHORTID"), *p)
}

func TestTimeoutDeadline(t *testing.T) {
	p := &MyFlow{}
	p = Prepare(p).(*MyFlow)
	log.Printf("EXPRESS: %#v", *p)
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	var retctx context.Context
	go func() {
		retctx = p.Execute(context.WithValue(ctx, "sleep", 10))
	}()
	select {
	case <-time.After(5 * time.Second):
		log.Println("overslept")
	case <-ctx.Done():
		log.Println(ctx.Err()) // prints "context deadline exceeded"
	}
	//log.Printf("RESULTS: %#v - UUID:%#v, SHORTID:%#v - %#v", retctx, retctx.Value("UUID"), retctx.Value("SHORTID"), *p)
}

func TestTimeoutOverslept(t *testing.T) {
	p := &MyFlow{}
	p = Prepare(p).(*MyFlow)
	log.Printf("EXPRESS: %#v", *p)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	var retctx context.Context
	go func() {
		retctx = p.Execute(context.WithValue(ctx, "sleep", 10))
	}()
	select {
	case <-time.After(3 * time.Second):
		log.Println("overslept")
	case <-ctx.Done():
		log.Println(ctx.Err()) // prints "context deadline exceeded"
	}
	//log.Printf("RESULTS: %#v - UUID:%#v, SHORTID:%#v - %#v", retctx, retctx.Value("UUID"), retctx.Value("SHORTID"), *p)
}

func TestTimeoutNone(t *testing.T) {
	p := &MyFlow{}
	p = Prepare(p).(*MyFlow)
	log.Printf("EXPRESS: %#v", *p)
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	var v = make(chan context.Context)
	go func() {
		v <- p.Execute(ctx)
	}()
	select {
	case <-time.After(3 * time.Second):
		log.Println("overslept")
	case <-ctx.Done():
		log.Println(ctx.Err()) // prints "context deadline exceeded"
	case retctx := <-v:
		log.Printf("RESULTS: %#v - UUID:%#v, SHORTID:%#v - %#v", retctx, retctx.Value("UUID"), retctx.Value("SHORTID"), *p)
	}
}
