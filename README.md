# ExeCon #


*I want to change the name of this project from _execon_ to _SFlow_; which stands for SFlow - Structured Flow based on Flow Based Programming where the graph is defined as a Go Structure*



I was listening to a podcast about the Apollo Guidance Computer. I do not have a complete
understanding about how it was implemented, however, I had one take away.

    There were two layers to the system. The first was an immutable core and the second
    was a mutable application. The core was implemented in rope memory so making changes
    in space was impossible. I believe the mutable part consisted of data and flow.

While one can make the same distinction between firmware and OS+application the notion
of immutability can be extended to firmware+OS (See ChromeOS, CoreOS, and to some extent NixOS) 
and Application.

Other examples of immutable infrastructure:

    firmware+OS+Container(see Docker and Rocket)+application

    firmware+OS+Container(see Docker and Rocket)+services

When the application is divided into services then the services can be stitched back together
much the same way that the Apollo system layer 2 did. 

Whether I'm correct about the analogs between the AGS and the application space. We know that
Flow Based Programming works. (see NoFlow and FlowHub). In that implementation the Tasks
are separate threads, the tasks communicate through queued channels, no shared state, 
the task nodes and channels make up a graph which the node wrappers use to move data
around the node network.

The current implementation of execon places all of this information directly in a struct
that the runner uses to execute one event at a time; although it should be thread safe (untested).

The struct is useful because it's:

- an abstraction that could be converted to a graph
- a lot of it is compiled so not everything needs to be tested at runtime
- It is possible to change the routing and some input/output from a JSON import/export of the instance


# Usage #

TBD

# Examples #

I used this tool to build an application that executes SQL, formats the results, displays it on the screen or 
exports it into a file, and optionally emails the file to a set of recipients using Mailgun.

Here is the "structure" which is also the flow.

```
type report struct {
        Init         execon.ExeConTask `params:"FIRST"`
        Open         execon.ExeConTask `switch:"{\"OpenXLS\":\"OpenXLS\", \"Else\":\"OpenOutFile\"}"`
        OpenXLS      execon.ExeConTask `switch:"{\"Next\":\"Connect\"}"`
        OpenOutFile  execon.ExeConTask `switch:"{\"Next\":\"Connect\"}"`
        Connect      execon.ExeConTask
        Ping         execon.ExeConTask `switch:"{\"Schema\":\"Schema\", \"Else\":\"MultiExec\"}"`
        Schema       execon.ExeConTask
        Each         execon.ExeConTask `switch:"{\"Next\":\"Finish\"}"`
        SchemaTable  execon.ExeConTask
        MultiExec    execon.ExeConTask `switch:"{\"Single\":\"Exec\", \"Next\":\"Close\"}"`
        Exec         execon.ExeConTask
        TSV          execon.ExeConTask `switch:"{\"MULTI\":\"DONE\", \"Next\":\"Close\"}"`
        CSV          execon.ExeConTask `switch:"{\"MULTI\":\"DONE\", \"Next\":\"Close\"}"`
        HTML         execon.ExeConTask `switch:"{\"MULTI\":\"DONE\", \"Next\":\"Close\"}"`
        Table        execon.ExeConTask `switch:"{\"MULTI\":\"DONE\", \"Next\":\"Close\"}"`
        XLS          execon.ExeConTask `switch:"{\"MULTI\":\"DONE\", \"Next\":\"Close\"}"`
        Close        execon.ExeConTask `switch:"{\"CloseXLS\":\"CloseXLS\", \"Else\":\"CloseOutFile\"}"`
        CloseXLS     execon.ExeConTask `switch:"{\"Next\":\"MailFile\"}"`
        CloseOutFile execon.ExeConTask `switch:"{\"Next\":\"MailFile\"}"`
        MailFile     execon.ExeConTask
        Finish       execon.ExeConTask
        Error        execon.ExeConTask
        SQLError     execon.ExeConTask
        FuncLib
}
```

Since ExeCon uses imterfaces... I need a little of this:

```
func (p *report) Execute(ctx context.Context) context.Context {
        log.Printf("Running: ...")
        return execon.Execute(context.WithValue(ctx, "SELF", p))
}
```

One interesting aspect is the branching based on the switch in the structure tags. This should be self explanatory.

```
func (f FuncLib) FnOpen(ctx context.Context) (context.Context, string) {
        outFile := "{{.OutFile}}"
        log.Printf("Open: ... %v", outFile)
        if outFile == "" {
                return context.WithValue(ctx, "outfile", os.Stdout), "Next"
        }
        return ctx, "Open{{.Format}}"
}

func (f FuncLib) FnOpenXLS(ctx context.Context) (context.Context, string) {
        return context.WithValue(ctx, "outfile", xlsx.NewFile()), "Next"
}

func (f FuncLib) FnOpenOutFile(ctx context.Context) (context.Context, string) {
        fh, err := os.Create(ctx.Value("outfilename").(string))
        if err != nil {
                return context.WithValue(ctx, "err", err), "Error"
        }
        return context.WithValue(ctx, "outfile", fh), "Next"
}
```

The Init step is interesting... In my example the PARAMS are used when calling the FnInit() method. Notice, in the gist below, that the FnInit() method accepts the CTX param but 
also accepts "firstStep" which correlates to the only field in the "params" tag.  The framework wulls the value from the CTX and puts it in the call.

```
func (f FuncLib) FnInit(ctx context.Context, firstStep string) (context.Context, string) {
        ctx = context.WithValue(ctx, "now", time.Now())
        next := "NEXT"
        if firstStep != "" {
                if tmp := ctx.Value(firstStep); tmp != nil {
                        next = tmp.(string)
                        log.Printf("FirstStep: ... %v, NEXT=%v", firstStep, next)
                }
        }
 
   . . . 
```

This specific detail is important because it can be used recursivly.

```
func (f FuncLib) FnEach(ctx context.Context) (context.Context, string) {
    . . .
    for rows.Next() {
        . . .
        // update the context
                ectx := context.WithValue(ctx, "schema", *schema)
                ectx = context.WithValue(ectx, "tablename", *name)
                fmt.Printf("TABLE: %s.%s\n", *schema, *name)
        // configure to enter into the struct-flow at the FIRST step
                ectx = context.WithValue(ectx, "FIRST", "SchemaTable")
        // execute the structure
                (ctx.Value("SELF")).(*report).Execute(ectx)

        }
    . . .
}
```

Here's how everything get's kicked off in the main(). Since I'm using context.Context I can time the execution and interrupt it if it times out.

```
func main() {
        p := &report{}
        p = execon.Prepare(p).(*report)
        ctx, cancel := context.WithTimeout(context.Background(), 3*time.Minute)
        defer cancel()
        var v = make(chan context.Context)
        go func() {
                v <- p.Execute(ctx)
        }()
        select {
        case <-time.After(3 * time.Minute):
                log.Println("something went very wrong and the report thread must have deadlocked")
        case <-ctx.Done():
                log.Println(ctx.Err()) // prints "context deadline exceeded"
        case retctx := <-v:
                log.Printf("Report ID: %v", retctx.Value("UUID"))
                log.Printf("Elapsed time: %v", retctx.Value("TotalElapsedTime"))
        }
}
```

## NOTES ##
- This example does not have a DEFER method but it should. 
- You have to be careful with global variables
- I would like to add FORK or some sort of async/concurrent execution
- during the registration I want to add a callback or two in order to capture metrics.
- boyscout the amount of console output and logging


# Copyright #

Copyright (c) 2015, One Off Code LLC., All Rights Reserved

# LICENSE #

MIT