// Copyright (2) 2015, One Off Code LLC., All Rights Reserved

package execon

// SFlow - Structured Flow based on Flow Based Programming where the graph is defined as a Go Structure
//

import (
	"encoding/json"
	"fmt"
	"log"
	"reflect"
	"strings"
	"time"

	"github.com/pborman/uuid"

	"golang.org/x/net/context"
)

// some helper type declarations because I read somewhere that this is preferred.
type SwitchKey string
type SwitchValue string
type SwitchMap map[string]string
type TaskParam []string
type ParamValues []reflect.Value

// ExeConFunc is a function prototype signature
//type ExeConFunc func(context.Context, ...interface{}) (context.Context, SwitchValue)
type ExeConFunc reflect.Value

// ExeConTask is a single node/task signature
// each field has a default value and behavior
type ExeConTask struct {
	Func        ExeConFunc    // default: use the fieldname prefixed with "Fn"
	Params      ParamValues   // default: empty
	Switch      SwitchMap     // default: {"ERROR":"_ERROR_", "DONE":"_EXIT_", "NEXT":"_NEXT_"}
	ElapsedTime time.Duration // used by the framework to record the duration
}

// ExeCon is the interface entrance to the workflow manager
type ExeCon interface {
	Execute(context.Context) context.Context
}

type ExeConGlobal interface {
	Register(context.Context) context.Context
}

var (
	defaultSwitch = SwitchMap{"ERROR": "ERROR", "DONE": "EXIT", "NEXT": "NEXT", "EXIT": "EXIT"}
)

// toValueArray is going to convert the params tag from an array of strings to an
// array of ParamValues(reflected)... right now everything is a string type. Empty
// strings are ignored.
func toValueArray(a []string) ParamValues {
	retval := ParamValues{}
	for _, v := range a {
		if v != "" {
			retval = append(retval, reflect.ValueOf(v))
		}
	}
	return retval
}

func captureSwitchMap(s, d SwitchMap) {
	for k, v := range s {
		d[k] = v
	}
}

// toSwitchMap is going to convert the "switch" struct tag into a map assuming that the
// switch string is a properly formatted JSON string. If the user did not present a switch
// tag then use the default config.
func toSwitchMap(s string) (SwitchMap, error) {
	retval := SwitchMap{}
	captureSwitchMap(defaultSwitch, retval)
	if s == "" {
		return retval, nil
	}
	tmp := SwitchMap{}
	err := json.Unmarshal([]byte(s), &tmp)
	if err != nil {
		return nil, err
	}
	captureSwitchMap(tmp, retval)
	return retval, nil
}

func toFunc(funcName string, parent reflect.Value) (ExeConFunc, error) {
	var err error
	methodValue := parent.MethodByName(funcName)
	if !methodValue.IsValid() {
		err = fmt.Errorf("toFunc('%#v') invalid or missing function (check 'Fn' method name prefix)", funcName)
	} else if methodValue.IsNil() {
		err = fmt.Errorf("toFunc('%#v') nil function", funcName)
	}
	return ExeConFunc(methodValue), err

}

// TODO add error response value and validate more info during init()
func initTask(p *ExeConTask, parent reflect.Value, sf reflect.StructField) error {
	var err error
	overrideFunc := sf.Tag.Get("funcname")
	if overrideFunc != "" {
		p.Func, err = toFunc(overrideFunc, parent)
	} else {
		p.Func, err = toFunc("Fn"+sf.Name, parent)
	}
	if err == nil {
		p.Params = toValueArray(strings.Split(sf.Tag.Get("params"), ","))
		p.Switch, err = toSwitchMap(sf.Tag.Get("switch"))
	}
	return err
}

// reflectValueDTD will convert the 'self' interface (a pointer or struct instance) into
// a reflect Value and reflect.Type so that it can be parsed etc as part of the SFlow.
func reflectValueDTD(self interface{}) (reflect.Value, reflect.Type) {
	val := reflect.Indirect(reflect.ValueOf(self))
	dtd := reflect.TypeOf(val.Interface())
	return val, dtd
}

// generateRevLookup inverts the reflect.Value.Field(n int) function so that field's index can be located by name.
// the index value is used elsewhere to locate the node in the other dictionary.
// I have considered converting this code to something else at runtime, however, reflection performance could be slow.
func generateRevLookup(self interface{}) map[string]int {
	retval := make(map[string]int)
	log.Printf("generate Rev Lookup")
	val, dtd := reflectValueDTD(self)
	for x := 0; x < val.NumField(); x++ {
		field := val.Field(x)
		if field.CanSet() {
			//log.Printf("Field #%v: %#v", x, val.Field(x))
			retval[dtd.Field(x).Name] = x
		}
	}
	return retval
}

type executeState struct {
	next      string
	prev      string
	step      int
	maxSteps  int
	start     time.Time
	revLookup map[string]int
	val       reflect.Value
	dtd       reflect.Type
	err       error
}

func (s *executeState) elapsed() time.Duration {
	return time.Since(s.start)
}

func initExecuteState(ctx context.Context) *executeState {
	self := ctx.Value("SELF")
	revLookup := generateRevLookup(self)
	val, dtd := reflectValueDTD(self)
	state := &executeState{"", "prev", 0, 100, time.Now(), revLookup, val, dtd, nil}
	return state
}

func (s *executeState) nextStep(task ExeConTask) {
	switch s.next {
	case "NEXT":
		s.prev = "prev"
		s.next = ""
		s.step++
	case "EXIT":
		break
	case "DONE":
		break
	case "ERROR":
		break
	default:
		//s.step = s.revLookup[s.next]
		//if s.step == 0 {
		//	s.next = task.Switch[s.next]
		//	if s.next == "" {
		//		s.next = task.Switch["Else"]
		//	}
		//	s.step = s.revLookup[s.next]
		//}
		tmp := s.next
		s.next = task.Switch[s.next]
		if s.next == "" {
			s.next = task.Switch["Else"]
		}
		s.step = s.revLookup[s.next]
		if s.step == 0 {
			s.next = tmp
			s.step = s.revLookup[s.next]
		}
	}
}

func (s *executeState) runner(ctx context.Context) context.Context {
	for s.prev != s.next && s.next != "DONE" && s.next != "ERROR" && s.maxSteps > 0 {
		s.prev = s.next
		field := s.val.Field(s.step)
		if field.Type().Name() == "ExeConTask" && field.CanSet() {
			task := field.Interface().(ExeConTask)
			log.Printf("STEP #%#v - %#v", s.step, s.dtd.Field(s.step).Name)
			if !reflect.Value(task.Func).IsValid() {
				log.Printf("func is nil, exiting")
				s.next = "DONE"
				break
			}
			//log.Printf("Running Field #%v: %#v", s.step, s.val.Field(s.step))
			params := []reflect.Value{}
			params = append(params, reflect.ValueOf(ctx))
			if len(task.Params) > 0 {
				params = append(params, task.Params...)
			}
			//log.Printf("PARAMS: %#v", params)
			start := time.Now()
			if !reflect.Value(task.Func).IsValid() {
				log.Printf("function does not exists '%s'", "TBD")
				break
			}
			retvals := reflect.Value(task.Func).Call(params)
			task.ElapsedTime = time.Since(start)
			s.val.Field(s.step).Set(reflect.ValueOf(task))
			ctx = retvals[0].Interface().(context.Context)
			s.next = retvals[1].String()
			log.Printf("STEP #%#v - %#v - %#v - %#v", s.step, s.dtd.Field(s.step).Name, "returned", s.next)
			s.nextStep(task)
		} else {
			s.next = "DONE"
		}
		s.maxSteps--
	}
	if s.maxSteps <= 0 {
		s.err = fmt.Errorf("shortcut exit; exceeded max steps")
	}
	if s.prev == s.next {
		s.err = fmt.Errorf("shortcut exit; next step not found- %s", s.next)
	}
	return ctx
}

// Execute will reflect the SELF value instance in the context.
// Loop over all of the public struct fields looking for the public/ExeConFunc(s)
// at each ExeConFunc
// (a) construct the param list
// (b) perform a relect.Call()
// (c) test the results against the select data and change the task index as indicated each task has defaults
// (d) record the duration of the each task
// (e) compute the private elapsed time
// FIXME add error return field
func Execute(ctx context.Context) context.Context {
	state := initExecuteState(ctx)
	// FIXME detect nested ID and log
	id := uuid.New()
	ctx = context.WithValue(ctx, "UUID", id)
	log.Printf("context %#v", ctx)
	ctx = context.WithValue(ctx, "SHORTID", id[:6])
	log.Printf("context %#v", ctx)
	ctx = state.runner(ctx)

	if state.err != nil {
		// FIXME return error instead of printing
		log.Printf("ERROR: %v", state.err)
	}
	return context.WithValue(ctx, "TotalElapsedTime", state.elapsed())
}

// RegisterMethods is going to iterate over all of the global public methods and add them
// to the ???
// FIXME there is an if statement that is incomplete and the data is not actually being stored/used
func RegisterMethods(i ExeCon) {
	log.Printf("registering methods...")
	globalType := reflect.TypeOf((*ExeConGlobal)(nil)).Elem()
	val, _ := reflectValueDTD(i)
	for x := 0; x < val.NumField(); x++ {
		field := val.Field(x)
		//log.Printf(">>>>> %#v -%#v - %#v", x, field.Type().Name(), val.Field(x), field.Type().Implements(globalType))
		if field.Type().Implements(globalType) {
			// FIXME
		}
	}
}

// RegisterTasks is going to iterate over all of the public fields in the ExeCon structure
// and precompute the name and step position so that the executor can navigate the namespace.
func RegisterTasks(i ExeCon) {
	log.Printf("registering tasks...")
	val, dtd := reflectValueDTD(i)
	for x := 0; x < val.NumField(); x++ {
		field := val.Field(x)
		if field.Type().Name() == "ExeConTask" {
			//log.Printf("registerTask #%v: %#v", x, val.Field(x))
			p := val.Field(x).Interface().(ExeConTask)
			err := initTask(&p, val, dtd.Field(x))
			if err != nil {
				log.Printf("ERROR: %v", err)
			}
			val.Field(x).Set(reflect.ValueOf(p))
		}
	}
}

// Prepare returns a generated instance of the structure passed in. The structure must
// be
func Prepare(i ExeCon) ExeCon {
	log.Printf("preparing")
	RegisterMethods(i)
	RegisterTasks(i)
	return i
}
